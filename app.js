const express = require("express");



let app = express();


app.get("/", function(req,res) {
  res.sendFile(__dirname + "/index.html");
});

app.get("/webhook", function(req,res) {
    res.status(200).send("");
});

app.listen(3000, function() {
  console.log("server started...");
});
